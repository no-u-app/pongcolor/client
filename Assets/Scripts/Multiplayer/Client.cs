using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

using UnityEngine;

class Client
{
    public Client()
    {
        try
        {
            string ip = "127.0.0.1";
            string message = "Test payload!";

            int port = 13000;
            TcpClient client = new TcpClient(ip, port);

            byte[] data = System.Text.Encoding.ASCII.GetBytes(message);

            NetworkStream stream = client.GetStream();

            stream.Write(data, 0, data.Length);

            Debug.Log("Sent: " + message);

            data = new byte[256];

            string res = String.Empty;

            int bytes = stream.Read(data, 0, data.Length);
            res = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
            Debug.Log("Received: " + res);

            stream.Close();
            client.Close();
        }
        catch (ArgumentNullException e)
        {
            Debug.Log("ArgumentNullException: " + e);
        }
        catch (SocketException e)
        {
            Debug.Log("SocketException: " + e);
        }
    }
}
