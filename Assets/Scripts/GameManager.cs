﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    [Header("Ball")]
    public GameObject ball;
    [Header("Player 1")]
    public GameObject Player1control;
    public GameObject player1goal;
    [Header("Player 2")]
    public GameObject player2control;
    public GameObject player2goal;
    [Header("Score UI")]
    public GameObject Player1text;
    public GameObject Player2Text;

    private int Player1Score;
    private int Player2Score;

    public GameManager()
    {
        Client client = new Client();
    }

    public void Player1Scored()
    {
        Player1Score++;
        Player1text.GetComponent<TextMeshProUGUI>().text = Player1Score.ToString();
        ResetPosition();
    }

    public void Player2Scored()
    {
        Player2Score++;
        Player2Text.GetComponent<TextMeshProUGUI>().text = Player2Score.ToString();
        ResetPosition();
    }

    private void ResetPosition()
    {
        ball.GetComponent<Ball>().Reset();
        Player1control.GetComponent<PlayerControl>().Reset();
        player2control.GetComponent<PlayerControl>().Reset();
    }
}
